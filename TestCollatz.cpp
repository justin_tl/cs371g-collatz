// ---------------
// TestCollatz.cpp
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----------------
// max_cycle_length
// ----------------

TEST(CollatzFixture, max_cycle_length_0) {
    ASSERT_EQ(max_cycle_length(1, 10), 20u);}

TEST(CollatzFixture, max_cycle_length_1) {
    ASSERT_EQ(max_cycle_length(100, 200), 125u);}

TEST(CollatzFixture, max_cycle_length_2) {
    ASSERT_EQ(max_cycle_length(201, 210), 89u);}

TEST(CollatzFixture, max_cycle_length_3) {
    ASSERT_EQ(max_cycle_length(900, 1000), 174u);}

TEST(CollatzFixture, max_cycle_length_4) {
    ASSERT_EQ(max_cycle_length(30, 100), 119u);}

TEST(CollatzFixture, max_cycle_length_5) {
    ASSERT_EQ(max_cycle_length(600, 700), 145u);}

TEST(CollatzFixture, max_cycle_length_6) {
    ASSERT_EQ(max_cycle_length(700, 800), 171u);}
TEST(CollatzFixture, max_cycle_length_7) {
    ASSERT_EQ(max_cycle_length(750, 850), 153u);}
TEST(CollatzFixture, max_cycle_length_8) {
    ASSERT_EQ(max_cycle_length(75, 85), 111u);}
TEST(CollatzFixture, max_cycle_length_9) {
    ASSERT_EQ(max_cycle_length(11, 50), 112u);}
TEST(CollatzFixture, max_cycle_length_10) {
    ASSERT_EQ(max_cycle_length(90, 100), 119u);}
TEST(CollatzFixture, max_cycle_length_11) {
    ASSERT_EQ(max_cycle_length(300, 500), 144u);}
TEST(CollatzFixture, max_cycle_length_12) {
    ASSERT_EQ(max_cycle_length(200, 400), 144u);}
TEST(CollatzFixture, max_cycle_length_13) {
    ASSERT_EQ(max_cycle_length(9, 60), 113u);}
TEST(CollatzFixture, max_cycle_length_14) {
    ASSERT_EQ(max_cycle_length(600, 900), 179u);}
TEST(CollatzFixture, max_cycle_length_15) {
    ASSERT_EQ(max_cycle_length(80, 700), 145u);}
TEST(CollatzFixture, max_cycle_length_16) {
    ASSERT_EQ(max_cycle_length(30, 1000), 179u);}
TEST(CollatzFixture, max_cycle_length_17) {
    ASSERT_EQ(max_cycle_length(94, 500), 144u);}
TEST(CollatzFixture, max_cycle_length_18) {
    ASSERT_EQ(max_cycle_length(92, 100), 119u);}
TEST(CollatzFixture, max_cycle_length_19) {
    ASSERT_EQ(max_cycle_length(900, 10000), 262u);}

