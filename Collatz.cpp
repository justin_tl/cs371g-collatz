// -----------
// Collatz.cpp
// -----------

// --------
// includes
// --------

#include <cassert> // assert

#include "Collatz.hpp"

using namespace std;
// ----------------
// max_cycle_length
// ----------------

int cache[1000000];
// ----
// main
// ----
unsigned max_cycle_length (unsigned i, unsigned j) {
    // <your code>
    if (i > j) {
        unsigned temp = i;
        i = j;
        j = temp;
    }
    unsigned max = 0;
    unsigned c = 1;
    long n = 0;
    if(i < (j/2))
        i = j/2;
    for(unsigned k = i; k <= j; k++){
    
        
        c = 1;
        n = k;

            while (n > 1) {
                if(n < 1000000 && cache[n] != 0) {
                    c += cache[n] - 1;
                    n = 1;
                }
                else {
                if ((n % 2) == 0)
                    n = (n / 2);
                else {
                    n = n + (n >> 1) + 1;
                    ++c;
                }
                ++c;}
            }
                if(k < 1000000)
                    cache[k] = c;

    if(c > max) {
        max = c;
    }
        
    }
    return max;}

